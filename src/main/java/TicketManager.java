/**
 * Created by RENT on 2017-08-31.
 */
public interface TicketManager {
    Ticket generateTicket(Gate gate);
    boolean tryValidate(Gate gate, int ticketId);
}
