import java.util.Observer;
import java.util.Optional;

/**
 * Created by RENT on 2017-08-31.
 */
public class Gate implements Notifiable{
    private int gateId;
    private boolean isOpen;
    private TicketManager server;

    public Gate(int gateId, TicketManager server) {
        this.gateId = gateId;
        this.server = server;
    }
    public void notifyAbout(Object o) {

    }

    public Optional<Ticket> generateTicket (String registrationNumber){
       Ticket ticket = new Ticket(registrationNumber);
        ticket = server.generateTicket(this);
        return ticket;
    }

    public boolean checkIn(String registrationNumber){
    //    Ticket ticket = generateTicket(registrationNumber);
        return false;
    }

    public boolean checkOut(int ticketId, String registrationNumber){
        return isOpen;
    }
}
