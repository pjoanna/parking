import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * Created by RENT on 2017-08-31.
 */
public class GateServer implements TicketManager{
    private int ticketCounter = 0;
    private int gateCounter = 0;
    private Map<String, Parking> nameParkingMap;
    private Map<Gate, Parking> gateParkingMap;
    private Rate rate;

    public Ticket generateTicket(Gate gate) {
        return null;
    }

    public boolean tryValidate(Gate gate, int ticketId) {
        return false;
    }

    public void addGate(int gateId, String parkingName){
       // gateParkingMap.put();
    }

    public void addParking (String name, int capacity){
//        nameParkingMap.put(name, Parking);
    }

    public Ticket generateTicket(String regNumber){
        Ticket ticket = new Ticket(ticketCounter++, Timestamp.valueOf(LocalDateTime.now()).getTime());
        return ticket;
    }

    public void createGate(){
        Gate gate = new Gate(gateCounter++, this);
        // po stworzeniu dodaj do mapy
    }
}


// ta klasa jest odpowiedzialna za obsługę bramek
// dodawanie ticketu gdy pojazd wjeżdża na parking
// usuwanie (archiwizowanie) ticketu gdy pojazd wyjeżdża z parkingu
// naliczanie opłat w zależności od czasu
// przechowuje informacje(?) o pojazdach