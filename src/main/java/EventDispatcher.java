/**
 * Created by RENT on 2017-08-31.
 */
public class EventDispatcher {
    private static EventDispatcher ourInstance = new EventDispatcher();

    public static EventDispatcher getInstance() {
        return ourInstance;
    }

    private EventDispatcher() {
    }
}
