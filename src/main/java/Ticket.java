import java.time.LocalDateTime;

/**
 * Created by RENT on 2017-08-31.
 */
public class Ticket {
    private int id;
    private LocalDateTime timestampIn;
    private long timestampOut;
    private long timestampPay;
    private boolean paid;
    private double amountToPay;
    private double converterInfo;
    private int idCounter = 0;

    public Ticket(){
        this.id = idCounter++;
        this.timestampIn = LocalDateTime.now();
        this.paid = false;
    }

    @Override
    public String toString() {
        return id + " czas wjazdu: " + timestampIn + " do zapłaty " + amountToPay ;
    }
}
